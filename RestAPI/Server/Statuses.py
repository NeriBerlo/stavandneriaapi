
### INFO ###
INFO_IN_PROGRESS = "In Progress"
INFO_DONE = "Done"
INFO_FAILED = "Failed"
INFO_INVALID = "Invalid"

### ERROR ###
## Compiler.py
# Error of max compilations at once
ERROR_MAX_COMPILATIONS = "Max amount of compilations are running right now"

# Compilation.py
# Error message format when the request is invalid
ERROR_MESSAGE_MESSAGE_FORMAT = "Invalid request. Required fields: {}"