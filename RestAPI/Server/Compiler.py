import os
import threading
import random
import time

import Statuses

def create_file(path, wait_time, content):
    """
    Creates a file and sleeps, with the content given + a ready signal
    :param path: The path of the file to create
    :param wait_time: The amount of time to wait
    :param content: The content to write to the file
    """
    open(path, "w").close()
    time.sleep(wait_time)
    with open(path, "w") as f:
        f.write("ready\n" + content)


class FileManager:
    """
    A small class to use instead of a compiler
    """
    def __init__(self, path=None, max_compilations=5):
        """
        Init
        :param path: The files' directory path
        :param max_compilations: The maximum amount of compilations at the same time
        """
        if path:
            self.path = path
        else:
            self.path = os.path.join(os.getcwd(), "files")

        # Create directory if it doesn't exist
        if not os.path.isdir(self.path):
            os.mkdir(self.path)

        self.max_compilations = max_compilations
        self.compilations = []

    def iterate_compilations(self):
        """
        cleans the compilation list for new compile requests
        """
        new_compilations = []
        # If compilation is alive keep it in the list
        for compilation in self.compilations:
            if compilation.is_alive():
                new_compilations.append(compilation)
        self.compilations = new_compilations

    def compile(self, **kwargs):
        """
        Starts compiling a project
        :param kwargs: The parameters sent by the client
        :return: The unique identifier
        """
        # Update compilations
        self.iterate_compilations()
        # Check if max compilations achieved
        if len(self.compilations) < self.max_compilations:
            # Generate random id
            random_num = random.randint(0, 133713371337)
            # Create a full path
            full_path = os.path.join(self.path, kwargs["project_name"] + "__" + str(random_num))
            # Check it doesn't exist
            while os.path.isfile(full_path):
                random_num = random.randint(0, 133713371337)
                full_path = os.path.join(self.path, kwargs["project_name"] + "__" + str(random_num))
            # Create compilation thread
            compilation_thread = threading.Thread(target=create_file,args=(full_path, 15, kwargs["content"]))
            # Add compilation thread to list
            self.compilations.append(compilation_thread)
            # Start thread
            compilation_thread.start()
            return kwargs["project_name"] + "__" + str(random_num)
        else:
            raise RuntimeError(Statuses.ERROR_MAX_COMPILATIONS)

    def is_ready(self, name):
        """
        Check if a file is ready for download
        :param name: The file identifier
        :return: A status concerning it's readiness
        """
        # Check if the file exists
        full_path = os.path.join(self.path, name)
        if not os.path.isfile(full_path):
            return Statuses.INFO_INVALID

        data = None

        # Read if the file is ready
        with open(full_path) as f:
            data = f.readline()[:-1]

        if data is not None and len(data) != 0:
            if data == "ready":
                return Statuses.INFO_DONE
            else:
                return Statuses.INFO_INVALID
        return Statuses.INFO_IN_PROGRESS

    def get_file(self, name):
        """
        Returns the file data
        :param name: The identifier
        :return: The file data
        """
        full_path = os.path.join(self.path, name)
        data = None
        with open(full_path) as f:
            f.readline()
            data = f.read()
        return data
