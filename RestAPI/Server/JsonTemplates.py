"""
This file provides a method to prepare all the json dictionaries easily
"""

# A format of the way the date should be returned to the client
DATE_FORMAT                 = "%d.%m.%Y %H:%M"

# A dictionary of failure messages
ERROR_MESSAGE               = ("message",)

# The metadata of a project
PROJECT_METADATA            = ("project_name", "project_version", "last_edited", "creation_date")
# The metadata of a specific compilation
COMPILATION_METADATA        = ("compilation_identifier", "status", "project_metadata")

# The request that should be sent to the server when compiling
COMPILE_REQUEST_REQUEST     = ("user", "project_name", "version", "repository")
# The result that is returned from a compilation request
COMPILE_REQUEST_RESULT      = ("compilation_metadata",)

# The result of a download request
COMPILE_DOWNLOAD_RESULT     = ("compilation_metadata", "result")


def build_dict(fields, **kwargs):
    """
    Builds a dictionary from a tuple and keyword arguments.
    Checks all fields are assigned.
    :param fields: The tuple of the fields
    :param kwargs: The arguments in the tuple
    :return: If successful, returns a dictionary. If failed throws a KeyError
    """
    try:
        return_dict = {}
        for field in fields:
            return_dict[field] = kwargs[field]

        return return_dict
    except KeyError:
        raise KeyError("Not all fields have been assigned values. Fields: {}".format(fields))
