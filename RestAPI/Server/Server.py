from sanic import Sanic
from sanic_restful_api import Resource, Api

import Compilation
from http import HTTPStatus

app = Sanic(__name__)
api = Api(app)


class DefaultResource(Resource):
    async def get(self, *args, **kwargs):
        return {"message": "pong"}, HTTPStatus.OK


# Compilation Resources
api.add_resource(DefaultResource, "/")
api.add_resource(Compilation.CompilationRequest, "/compile/request")
api.add_resource(Compilation.CompilationSearch, "/compile/search")
api.add_resource(Compilation.CompilationDownload, "/compile/download/<identifier:string>")

if __name__ == "__main__":
    # TODO: run compiler server
    app.run(debug=True)