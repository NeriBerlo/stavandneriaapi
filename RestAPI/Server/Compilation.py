from sanic import request
from sanic_restful_api import Resource, reqparse
from datetime import datetime
from http import HTTPStatus
import JsonTemplates
import Compiler
import Statuses


man = Compiler.FileManager()

class CompilationRequest(Resource):
    """
    A class handling compilation requests
    """
    @staticmethod
    def check_valid_request(data):
        """
        Checks that the given JSON data is valid.
        :param data: The JSON data received from the client.
        :return: True if valid, False otherwise.
        """
        # If empty, it's invalid.
        # Check all fields exist
        fields = JsonTemplates.COMPILE_REQUEST_REQUEST
        return data is not None and all(field in data.keys() for field in fields) and len(data) == len(fields)

    def compile(self, user, project_name, version, repository):
        """
        Compiles a certain project.
        :param user: The user that initiated the compilation
        :param project_name: The project's name
        :param version: The project's version
        :param repository: The repository data
        :return: Compilation data to return to the client
        """
        try:
            identifier = man.compile(project_name=project_name,content=repository)
        except RuntimeError:
            identifier = None

        # build project metadata
        project_metadata = JsonTemplates.build_dict(JsonTemplates.PROJECT_METADATA,
                                                    project_name=project_name,
                                                    project_version=version,
                                                    last_edited=datetime.strftime(datetime.now(),
                                                                                  JsonTemplates.DATE_FORMAT),
                                                    creation_date=datetime.strftime(datetime.now(),
                                                                                    JsonTemplates.DATE_FORMAT))
        # build compilation metadata
        compilation_metadata = JsonTemplates.build_dict(JsonTemplates.COMPILATION_METADATA,
                                                        compilation_identifier=identifier,
                                                        status=Statuses.INFO_IN_PROGRESS if identifier else Statuses.INFO_FAILED,
                                                        project_metadata=project_metadata)
        # Return compilation info
        return JsonTemplates.build_dict(JsonTemplates.COMPILE_REQUEST_RESULT, compilation_metadata=compilation_metadata)

    async def post(self, *args, **kwargs):
        """
        Processes post requests to the /compile/request endpoint
        :return: JSON answer of compilation metadata.
        """
        # Get data
        data = args[0].json

        # Check data validity
        if self.check_valid_request(data):
            # Start compiling
            return self.compile(data["user"], data["project_name"], data["version"], data["repository"]), HTTPStatus.CREATED

        # Invalid format
        return JsonTemplates.build_dict(JsonTemplates.ERROR_MESSAGE,
                                        message=Statuses.ERROR_MESSAGE_MESSAGE_FORMAT.format(
                                            JsonTemplates.COMPILE_REQUEST_REQUEST)), HTTPStatus.BAD_REQUEST


class CompilationSearch(Resource):
    """
    A class handling search requests.
    """
    @staticmethod
    def check_valid_request(data):
        fields = ("")
        return True #if all(field in data.keys() for field in fields) and len(data) == len(fields) else False

    def search_compilation(self):
        return {"happy" : "tummy"}

    async def get(self, *args, **kwargs):
        """
        Processes get request to the /compile/search endpoint.
        :return: JSON answer of the search results
        """
        data = args[0].json
        if self.check_valid_request(data):
            return self.search_compilation(), HTTPStatus.OK
        return {"message": Statuses.ERROR_MESSAGE_MESSAGE_FORMAT.format("None")}, HTTPStatus.BAD_REQUEST


class CompilationDownload(Resource):
    """
    A class that handles download requests
    """
    def fetch_result(self, identifier):
        """
        Gets the file data needed to return
        :param identifier: The compilation identifier
        :return:
        """
        status = man.is_ready(identifier)
        return_json = JsonTemplates.build_dict(JsonTemplates.COMPILE_DOWNLOAD_RESULT,
                                               compilation_metadata=None,
                                               result=None)
        if status == Statuses.INFO_DONE:
            return_json["compilation_metadata"] = JsonTemplates.build_dict(JsonTemplates.COMPILATION_METADATA,
                                                                           compilation_identifier=identifier,
                                                                           status=status,
                                                                           project_metadata=None)
            return_json["result"] = man.get_file(identifier)
        else:
            return_json["compilation_metadata"] = JsonTemplates.build_dict(JsonTemplates.COMPILATION_METADATA,
                                                                           compilation_identifier=identifier,
                                                                           status=status,
                                                                           project_metadata=None)
            return_json["result"] = None
        return return_json

    async def get(self, *args, **kwargs):
        """
        Handles download requests
        """
        return self.fetch_result(kwargs["identifier"]), HTTPStatus.OK
