import requests
from shutil import make_archive
from Client.ClientConfiguration import *
import argparse
import os
import zipfile

class HTTPClient(object):
    """
    Wrapper to requests library. made for using SNAPI more comfortable
    """

    def __init__(self, server_ip, server_port):
        """
        :param server_ip: http server ip
        :type server_ip: C{str}
        :param server_port: http server port
        :type server_port: C{str}
        """
        self.http_address = "".join(("http://", server_ip, ':', server_port, '/'))

    def get(self, endpoint, param):
        """
        Sending GET query to the server
        :param endpoint: server endpoint
        :type endpoint: C{str}
        :param param: parameter for the query
        :type param: C{str}
        :return: get query response
        """
        return requests.get(self.http_address + endpoint + '/' + param)

    def post(self, endpoint, json):
        """
        Sending POST query to the server
        :param endpoint: server endpoint
        :type endpoint: C{str}
        :param json: json to send with the query
        :type: C{str(json)}
        :return: POST query response
        """
        return requests.post(self.http_address + endpoint, json=json)


class SNAPIClient(object):
    CLIENT = HTTPClient(SERVER_IP, SERVER_PORT)

    def __init__(self, repository_path, username, project_name, version):
        self.data = str(self.get_zip_data(repository_path))
        self.user_name = username
        self.project_name = project_name
        self.version = version

    @staticmethod
    def get_zip_data(path):
        """
        Zip the repository directory
        :param path: path to directory
        :return: buffer data of the zip file created and delete the zip file
        """
        make_archive(path, 'zip', path)
        data = ""
        with open("".join((path, ".zip")), 'rb') as zippi:
            data = zippi.read()
        return data

    def _create_compile_request_json(self):
        """
        Create json for post request in the follow format:
        {
            'user_name': user name,
            'project name': project name,
            'version' : version,
            'repository' : repository zip data
        }
        :return: the follow json file. 
        """
        return {USER_COMPILE_REQUEST_DATA[USER]: self.user_name,
                USER_COMPILE_REQUEST_DATA[PROJECT_NAME]: self.project_name,
                USER_COMPILE_REQUEST_DATA[VERSION]: self.version,
                USER_COMPILE_REQUEST_DATA[REPOSITORY]: self.data}

    def compile_request(self):
        """
        Sending the repository to compile
        :return: response answer
        """
        return SNAPIClient.CLIENT.post(COMPILE_REQUEST_ENDPOINT, self._create_compile_request_json())

    def search_request(self):
        pass

    @staticmethod
    def download_compilation(compilation_identifier):
        """
        Sending GET requests to get the compile data
        :return: response answer
        """
        return SNAPIClient.CLIENT.get(COMPILE_DOWNLOAD_ENDPOINT, compilation_identifier)


def parse_arg():
    arg_parser = argparse.ArgumentParser()

    subparser = arg_parser.add_subparsers(dest="command", required=True)

    compile_flag = subparser.add_parser('Compilation', help='Send repository to compile\nTry "Compilation -h"')
    compile_flag.add_argument('-r', '--repository', type=str, required=True, help='Repository dir path')
    compile_flag.add_argument('-u', '--user_name', type=str, required=True, help='user name')
    compile_flag.add_argument('-p', '--project_name', type=str, required=True, help='project name')
    compile_flag.add_argument('-v', '--project_version', type=str, required=True, help='project version')

    download_flag = subparser.add_parser('Download', help='Get compiled binaries with project identifier\nTry "Download -h"')
    download_flag.add_argument('-p', '--project_identifier', type=str, required=True, help='project identifier')
    return arg_parser.parse_args()


def main():
    args = parse_arg()

    # Manage Compilation:
    if args.command == 'Compilation':
        client = SNAPIClient(args.repository, args.user_name, args.project_name, args.project_version)
        return client.compile_request()

    # Manage download
    elif args.command == 'Download':
        return SNAPIClient.download_compilation(args.project_identifier)

    else:
        return "Bad usage"


if __name__ == '__main__':
   main()