import struct

### SERVER SETTINGS ###
# The IP for the compilation server
SERVER_IP = "104.155.93.171"
# The port used to conenct to the compilation server
SERVER_PORT = "1337"

### JSON REQUEST INDICES ###
USER = 0
PROJECT_NAME = 1
VERSION = 2
REPOSITORY = 3

### JSON STRUCTURES ###
# The data that needs to be sent for a compilation request
USER_COMPILE_REQUEST_DATA = ['user', 'project name', 'version', 'repository']
# The response returned when requesting compilation
SERVER_COMPILE_RESPONSE_DATA = "compilation_metadata"

### ENDPOINTS ###
COMPILE_REQUEST_ENDPOINT = "compile/request"
COMPILE_SEARCH_ENDPOINT = "compile/search"
COMPILE_DOWNLOAD_ENDPOINT = "compile/download"